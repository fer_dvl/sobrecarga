public class Principal{
	public static void main(String[] args) {
		System.out.println("==> CalculadoraInt --> " + engine_1((int)6, (int)5).calculate(5,5));
		System.out.println("==> CalculadoraLong --> " + engine_1((long) 8,(long)2).calculate(7,2));
		//System.out.println("==> CalculadoraInt --> " + (int) engine_1().calculate(5,5));
	}

	//Retorna un objeto de tipo "CalculadoraInt"
	private static CalculadoraInt engine_1(int a, int b){
		//x = a;
		return (x,y) ->  a * b; //Retorna una Lambda
		//x & y son variables locales
	}


	//Retorna un objeto de tipo "CalculadoraLong"
	private static CalculadoraLong engine_1(long a, long b){
		return (x,y) -> a - b;
	}
}